import math
import json
import logging
import requests
import pandas as pd
from typing import *
import kfserving


logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


class BaseTransformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.model_name = name
        self.predictor_url = predictor_host

    def preprocess(self, inputs: Dict[str, List[Dict]]) -> Dict:
        def per_instance_preprocess(instance):
            return instance

        return {"instances": list(map(per_instance_preprocess, inputs["instances"]))}

    async def predict(self, request: Dict) -> Dict:
        # This function should be untouched and just copied.
        req_body = json.dumps(request)
        response = requests.post(
            f"http://{self.predictor_url}.svc.cluster.local/v1/models/{self.model_name}:predict",
            data=req_body
        )
        logging.info(response.text)
        return response.json()
