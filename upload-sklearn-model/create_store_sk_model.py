import os
import boto3
from sklearn import linear_model
from joblib import dump, load
from dotenv import load_dotenv

load_dotenv()

MODEL_NAME = "sk-base-model"
# Model file has to be named "model.joblib"
MODEL_PATH = "mymodels/sk-base-model/model.joblib"

clf = linear_model.LinearRegression()
X, y = [[0, 0], [1, 1], [2, 2]], [0, 1, 2]
clf.fit(X, y)
dump(clf, MODEL_PATH)

client = boto3.client(
    "s3",
    endpoint_url="https://s3.cern.ch",
    aws_access_key_id = os.getenv("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key = os.getenv("AWS_SECRET_ACCESS_KEY")
)
bucket = "admon-is"
# Bucket object has to be versioned with number in path
bucket_object = f"{MODEL_NAME}/1/model.joblib"
client.upload_file(MODEL_PATH, bucket, bucket_object)
