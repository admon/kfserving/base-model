# base-model for KFServing Inference Service

## Prerequisits

* Use k8s secret for AWS keys, as shown in `upload-base-model/deploy.yaml`
* Link secret to service account, as shown in `upload-base-model/deploy.yaml`
* Use service account for loading model from S3, as shown in `upload-base-model/deploy.yaml`

## Getting Started
### Upload Model
To create a `SKLearn` model you have free choice. The only thing to keep in mind
is to use the same version as KFServing Server.
With this one: `gitlab-registry.cern.ch/admon/kfserving/sklearn:stable` version `scikit-learn==0.24.1` is used.
With a trained model, upload this then to S3, as shown in `upload-base-model/create-store-model.py`.

### Create a Transformer for pre and post-processing
As shown in folder `transformer`, you need to implement a `kfserving.KFModel` class
and can override the `preprocess`, `predict` and `postprocess` functions. But you only need
to implement the `preprocess` function yourself, the `predict` function can be copied from
this repo here and the `postprocess` function is optional.

#### Preprocess function
This function receives a dictionary with a list of instances.
Just in the same format as the `admonapi` will return it.
These have to be preprocessed, so that the model can call `predict()` on the data.

This is shown in `transformer/BaseTransformer.py` with the `preprocess()` function.   

#### Predict function
Don't touch it, just copy the `predict` function.

#### Postprocess function
This `postprocess` is an optional function to override. This function receives a
dictionary with the `predictions` key and a list of results. The return should have the
same shape.

### URLs generated for protocol version `v1`:
* Internal: `POST http://MODEL_NAME.NAMESPACE.svc.cluster.local/v1/models/MODEL_NAME:predict`
* External: `POST https://ml.cern.ch/v1/models/MODEL_NAME:predict` with header:
    * `Host: MODEL_NAME.NAMESPACE.example.com`
    * `Cookie: authservice_session=<authservice_session from https://ml.cern.ch>`
