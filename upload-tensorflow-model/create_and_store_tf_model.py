import os
import boto3
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from dotenv import load_dotenv

load_dotenv()

MODEL_NAME = "tf-base-model"
MODEL_PATH = "mymodels/..."

model = keras.Sequential([
    layers.Dense(4, input_shape=(1,), activation="relu"),
    layers.Dense(4, activation="relu"),
    layers.Dense(1),
])
model.save(MODEL_NAME)

client = boto3.client(
    "s3",
    endpoint_url="https://s3.cern.ch",
    aws_access_key_id = os.getenv("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key = os.getenv("AWS_SECRET_ACCESS_KEY")
)
bucket = "admon-is"
# Bucket object has to be versioned with number in path
bucket_object = f"{MODEL_NAME}/1/model.pb"
client.upload_file(MODEL_PATH, bucket, bucket_object)
